En tekst er trykte eller skrevne ord, setninger og avsnitt som er føyd sammen til en helhet, for eksempel i brev, fortellinger, skildringer, romaner, noveller, skuespill, dikt og så videre. Begrepet «tekst» kan også sikte til ordene til et vokalt musikkverk, eller til grunnspråk (motsatt av oversettelse). Ordet kan vise til innholdet av et skrift eller til skriftet selv.

Faktaboks
ETYMOLOGI: av norrønt textr, fra latin textus, grunnbetydning 'sammenføyning, vevning', se tekstil
Tekst kan også være et skriftledd eller utdrag av Bibelen, som grunnlag for en preken (som i dagens tekst).

I språkvitenskapen defineres tekst som en helhetlig, sammenhengende, språklig ytring med en bestemt kommunikativ funksjon. En tekst gir uttrykk for en kommunikativ intensjon, som å informere om noe, å oppfordre til handling eller å uttrykke talerens følelser. Videre består den av språklige tegn, som har et innhold og et uttrykk, og formidler på den måten referensielt innhold, det vil si informasjon om en reell, hypotetisk eller fiktiv verden. Tegnene er uttrykt i en viss modalitet, som skrift eller tale, og i et visst medium, som avis, bok, TV eller internett. Mange tekster inkluderer flere modaliteter, som bilder, musikk eller gestikk, og disse kalles da sammensatte eller multimodale tekster. De språklige (og eventuelt ikke-verbale) tegnene er knyttet sammen til en koherent helhet med en begynnelse og en slutt.

Endelig inngår en tekst i en sjanger, og forholder seg dermed til visse konvensjoner og normer knyttet til sjangeren. Den følger ikke nødvendigvis disse normene, men forstås på bakgrunn av sjangertilhørighet, slik at eventuelle brudd med sjangeren framstår som brudd (som da kan tolkes som manglende kompetanse eller kreativ eksperimentering).
