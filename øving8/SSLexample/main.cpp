#include "crack.hpp"
#include <iostream>

using namespace std;

int main() {

  string letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  string foundKey = "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6";
  string foundSalt = "Saltet til Ola";

  int iterations = 2048;
  int keyLength = 160 / 8;

  char password[3];

  for (char c1 : letters) {
    password[0] = c1;

    for (char c2 : letters) {
      password[1] = c2;

      for (char c3 : letters) {
        password[2] = c3;
        string generatedHash = Crack::hex(Crack::pbkdf2(password, foundSalt, iterations, keyLength));
        cout << generatedHash << ": " << password << endl;

        if (generatedHash == foundKey) {
          cout << "Password cracked: " << password << endl;
          return 0;
        }
      }
    }
  }

  return 0;
}
