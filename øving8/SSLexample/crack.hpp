#include <iomanip>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <sstream>
#include <string>

using namespace std;

class Crack {
public:
  /// Return hex string from bytes in input string.
  static string hex(const string &input) {
    stringstream hex_stream;
    hex_stream << std::hex << internal << setfill('0');
    for (auto &byte : input)
      hex_stream << setw(2) << (int)(unsigned char)byte;
    return hex_stream.str();
  }

  /// Return the SHA-1 hash from input (160-bit)
  static string sha1(const string &input) {
    string hash;
    hash.resize(160 / 8);
    SHA1((const unsigned char *)&input[0], input.size(), (unsigned char *)&hash[0]);
    return hash;
  }

  /// Return key from PBKDF2
  static string pbkdf2(const string &password, const string &salt, int iterations = 4096, int key_length = 256 / 8) {
    string key;
    key.resize(key_length);
    auto success = PKCS5_PBKDF2_HMAC_SHA1(&password[0], password.size(), (const unsigned char *)&salt[0], salt.size(), iterations, key_length, (unsigned char *)&key[0]);
    if (!success)
      throw runtime_error("openssl: error calling PBKCS5_PBKDF2_HMAC_SHA1");
    return key;
  }
};
