//@flow
import express from 'express';

import path from 'path';
import reload from 'reload';
import fs from 'fs';

const public_path = path.join(__dirname, '/../../client/public');
const crypto = require('crypto');
const app = express();

app.use(express.static(public_path));
app.use(express.json());

let users = {};

let registerUser = (username, serverHash, salt) => {
   if (users[username] != null) return null;

   let user = {
      [username]: {
         salt: salt,
         hash: serverHash,
      },
   };

   users = { ...users, ...user };
   return user;
};

// Logs a user in
app.post('/login', (req, res) => {
   const username = req.body.username;
   const clientHash = req.body.clientHash;

   let user = users[username];
   if (user == null) return res.json({ state: 'NOT AUTHORIZED' });

   crypto.pbkdf2(clientHash, user.salt, 4096, 512 / 32, 'SHA512', (error, serverHash) => {
      if (error) res.json({ state: 'ERROR' });
      user.hash == serverHash.toString('hex') ? res.json({ state: 'AUTHORIZED' }) : res.json({ state: 'NOT AUTHORIZED' });
   });
});

// Registers a new user
app.post('/register', (req, res) => {
   const username = req.body.username;
   const clientHash = req.body.clientHash;

   // Encrypts client hash, stores new user
   let salt = crypto.randomBytes(512).toString('base64');
   crypto.pbkdf2(clientHash, salt, 4096, 512 / 32, 'SHA512', (error, serverHash) => {
      if (error) res.json({ state: 'ERROR' });

      let result = registerUser(username, serverHash.toString('hex'), salt);
      if (result == null) return res.json({ state: 'USERNAME TAKEN' });

      res.json({ state: 'REGISTERED' });
   });
});

reload(app).then((reloader) => {
   app.listen(4000, (error: ?Error) => {
      if (error) console.error(error);
      console.log('Express server started');
      reloader.reload();
   });
});
