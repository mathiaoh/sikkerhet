const crypto = require('crypto');

class Token {
   constructor(id, group) {
      this.id = id;
      this.group = group;
      this.date = new Date().toISOString().replace(/[^0-9]/g, '');
      this.salt = crypto.randomBytes(16).toString('base64');
   }

   toString() {
      return this.id + '' + this.group + '' + this.date + '' + this.salt;
   }
}

module.exports = Token;
