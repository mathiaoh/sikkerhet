// @flow
import axios from "axios";

class Handler {
  sendData(username: string, clientHash: string, path: string) {
    //$FlowFixMe
    return axios({
      method: "POST",
      url: path,
      headers: {},
      data: {
        username: username,
        clientHash: clientHash,
      },
    });
  }

  checkToken(username: string, clientHash: string, token: string) {
    //$FlowFixMe
    return axios({
      method: "POST",
      url: "/token",
      headers: {
        authorization: token,
      },
      data: {
        username: username,
        clientHash: clientHash,
      },
    });
  }
}

export let handler = new Handler();
