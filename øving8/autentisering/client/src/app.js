// @flow
import ReactDOM from "react-dom";
import * as React from "react";
import { Component } from "react-simplified";

import { handler } from "./services";

import "./main.css";

const crypto = require("crypto");

class App extends Component {
  username: string = "";
  password: string = "";
  clientHash: string = "";

  login = () => {
    this.PBKDF2("/login");
  };

  register = () => {
    this.PBKDF2("/register");
  };

  // Encrypts password with PBKDF2 and sends to server
  PBKDF2 = (path) => {
    crypto.pbkdf2(
      this.password,
      window.location.hostname + window.location.pathname + this.username,
      1024,
      512 / 32,
      "SHA512",
      (error, hash) => {
        if (error) throw error;
        this.clientHash = hash.toString("hex");
        this.sendData(this.username, this.clientHash, path);
      }
    );
  };

  sendData(username, clientHash, path) {
    handler
      .sendData(this.username, this.clientHash, path)
      .then((res) => {
        let state = res.data.state;
        switch (state) {
          case "ERROR":
            alert("A fatal error occured");
            break;
          case "REGISTERED":
            alert("Succesfully registered");
            break;
          case "USERNAME TAKEN":
            alert("Username taken");
            break;
          case "AUTHORIZED":
            alert("Logging in...");
            break;
          case "NOT AUTHORIZED":
            alert("Invalid login credentials");
            break;
        }
        this.username = "";
        this.password = "";
      })
      .catch((error) => console.error(error));
  }

  handleUsernameChange = (event) => {
    this.username = event.target.value;
  };

  handlePasswordChange = (event) => {
    this.password = event.target.value;
  };

  render() {
    return (
      <div className="container">
        <input
          className="username"
          type="text"
          placeholder="username"
          value={this.username}
          onChange={this.handleUsernameChange}
        ></input>

        <input
          className="password"
          type="password"
          placeholder="password"
          value={this.password}
          onChange={this.handlePasswordChange}
        ></input>
        <button className="login" onClick={this.login}>
          Log in
        </button>
        <button className="register" onClick={this.register}>
          Register
        </button>
      </div>
    );
  }
}

const root = document.querySelector(".root");
if (root) ReactDOM.render(<App />, root);

export default App;
