#include <iostream>
using namespace std;

string rep(string origString) {
  size_t position = origString.find('&');
  while (origString.find('&', position) != string::npos) {
    origString = origString.replace(position, 1, "&amp");
    position = origString.find('&', position + 1);
  }
  position = origString.find('<');
  while (origString.find('<', position) != string::npos) {
    origString = origString.replace(position, 1, "&lt");
    position = origString.find('<', position + 1);
  }
  position = origString.find('>');
  while (origString.find('>', position) != string::npos) {
    origString = origString.replace(position, 1, "&gt");
    position = origString.find('>', position + 1);
  }

  return origString;
}

int main() {
  string string("Dette er en streng med: & < > && << >>");
  cout << string << endl;
  cout << rep(string) << endl;
}
