#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *replace(char *origString, char *rep, char *with) {
  char *result;
  char *ins_point;
  char *tmp;
  int len_front;
  int count;
  int len_rep = strlen(rep);
  int len_with = strlen(with);

  // Telle kor mykje vi må utvide
  ins_point = origString;
  for (count = 0; (tmp = strstr(ins_point, rep)); ++count) {
    ins_point = tmp + len_rep;
  }

  // Sette av plassen i minnet
  tmp = result = malloc(strlen(origString) + (len_with - len_rep) * count + 1);

  //om allokeringen feila avbryte vi
  if (!result)
    return NULL;

  while (count--) {
    ins_point = strstr(origString, rep);
    len_front = ins_point - origString;
    tmp = strncpy(tmp, origString, len_front) + len_front;
    tmp = strcpy(tmp, with) + len_with;
    origString += len_front + len_rep;
  }
  strcpy(tmp, origString);
  return result;
}

char *change(char *message) {
  message = replace(message, "&", "&amp");
  message = replace(message, "<", "&lt");
  message = replace(message, ">", "&gt");
  return message;
}

int main() {
  char *message = "Dette er en streng med: & < > && << >>";
  printf("%s\n", message);
  printf("%s\n", change(message));
}
